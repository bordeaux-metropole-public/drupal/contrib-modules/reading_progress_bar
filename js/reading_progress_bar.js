/**
 * @file
 * JS handler for reading progress bar.
 */
(($) => {
  Drupal.behaviors.reading_progress_bar = {
    attach() {
      const progressBar = $('progress');
      const autoHideDelay = progressBar.data('hide-delay') || 3000;
      const minimumDocumentScreenRatio = progressBar.data('min-ratio') || 2;
      let isVisible = progressBar.is(':visible');
      function setupProgressBar() {
        /* Set the max scrollable area */
        const winHeight = $(window).height();
        const docHeight = $(document).height();
        const max = docHeight - winHeight;
        progressBar.attr('max', max);
        if (docHeight / winHeight > minimumDocumentScreenRatio) {
          progressBar.show();
          isVisible = true;
        } else {
          progressBar.hide();
          isVisible = false;
        }
      }
      function updateProgressBarValue() {
        const value = $(window).scrollTop();
        progressBar.attr('value', value);
      }
      // Initial setup.
      setupProgressBar();
      // Refresh setup when the page is fully loaded.
      window.addEventListener('load', setupProgressBar);
      // Refresh setup when the viewport is resized.
      window.addEventListener('resize', setupProgressBar);
      let autoHideTimer = null;
      $(document).on('scroll', () => {
        // Update progress bar if visible only.
        if (isVisible) {
          updateProgressBarValue();
          if (progressBar.hasClass('hidden')) {
            progressBar.removeClass('hidden');
          }
          if (autoHideDelay > 0) {
            if (autoHideTimer) {
              clearTimeout(autoHideTimer);
            }
            autoHideTimer = setTimeout(() => {
              progressBar.addClass('hidden');
            }, autoHideDelay);
          }
        }
      });
    },
  };
})(jQuery);

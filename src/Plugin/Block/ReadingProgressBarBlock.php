<?php

namespace Drupal\reading_progress_bar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Template\Attribute;
use Drupal\reading_progress_bar\ReadingProgressBarConstants as Constants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Reading Progress Bar' block.
 *
 * @Block(
 *  id = "reading_progress_bar_block",
 *  admin_label = @Translation("Reading Progress Bar block"),
 * )
 */
class ReadingProgressBarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'height' => Constants::DEFAULT_HEIGHT,
      'color' => Constants::DEFAULT_COLOR,
      'background_color' => Constants::DEFAULT_BACKGROUND_COLOR,
      'background_transparent' => 1,
      'border' => Constants::DEFAULT_BORDER,
      'minimum_ratio' => Constants::DEFAULT_MINIMUM_RATIO,
      'hide_delay' => Constants::DEFAULT_HIDE_DELAY,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['height'] = [
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#description' => t('You can enter css height for the reading bar.'),
      '#default_value' => $this->configuration['height'],
    ];
    $form['color'] = [
      '#type' => 'color',
      '#title' => t('Color'),
      '#description' => t('You can enter css color for the reading bar.'),
      '#default_value' => $this->configuration['color'],
    ];
    $form['background_transparent'] = [
      '#type' => 'checkbox',
      '#title' => t('Transparent Background'),
      '#description' => t('Choose this option to make background transparent instead of a background color.'),
      '#default_value' => $this->configuration['background_transparent'],
    ];
    $form['background_color'] = [
      '#type' => 'color',
      '#title' => t('Background Color'),
      '#description' => t('You can enter css background color for the reading bar.'),
      '#default_value' => $this->configuration['background_color'],
      '#states' => [
        'invisible' => [
          ':input[name="settings[background_transparent]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['border'] = [
      '#type' => 'textfield',
      '#title' => t('Border'),
      '#description' => t('You can enter css border for the reading bar.'),
      '#default_value' => $this->configuration['border'],
    ];
    $form['minimum_ratio'] = [
      '#type' => 'number',
      '#title' => t('Minimum document/screen ratio'),
      '#description' => t('Minimum ratio below which the reading progress bar should not be displayed. A value below 1 will disable this feature.'),
      '#default_value' => $this->configuration['minimum_ratio'],
    ];
    $form['hide_delay'] = [
      '#type' => 'number',
      '#title' => t('Auto-hide delay (in ms)'),
      '#description' => t('Will automatically hide the reading progress bar if no scrolling is detected within the specified timeframe. A value of 0 will disable this feature.'),
      '#default_value' => $this->configuration['hide_delay'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['color'] = $form_state->getValue('color');
    $this->configuration['background_color'] = $form_state->getValue('background_color');
    $this->configuration['background_transparent'] = $form_state->getValue('background_transparent');
    $this->configuration['border'] = $form_state->getValue('border');
    $this->configuration['minimum_ratio'] = $form_state->getValue('minimum_ratio');
    $this->configuration['hide_delay'] = $form_state->getValue('hide_delay');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $class = 'reading-progress-bar';
    $height = $this->configuration['height'];
    $color = $this->configuration['color'];
    $background_color = $this->configuration['background_color'];
    $background_transparent = $this->configuration['background_transparent'];
    if (!empty($background_transparent)) {
      $background_color = 'transparent';
    }
    $border = $this->configuration['border'];

    $attributes = new Attribute(['class' => [$class, 'hidden']]);

    $minimum_ratio = $this->configuration['minimum_ratio'];
    $attributes['data-min-ratio'] = $minimum_ratio;
    $hide_delay = $this->configuration['hide_delay'];
    $attributes['data-hide-delay'] = $hide_delay;

    $inline_style = "progress.$class{color: $color; height: $height;  background-color: $background_color; border: $border;} progress.$class::-webkit-progress-value {background-color: $color;} progress.$class::-moz-progress-bar {background-color: $color;} progress.$class::-webkit-progress-bar {background-color: $background_color;}";

    $build['reading_progress_bar'] = [
      '#theme' => 'reading_progress_bar',
      '#attributes' => $attributes,
      '#inline_style' => $inline_style,
      '#attached' => [
        'library' => [
          'reading_progress_bar/reading_progress_bar',
        ],
      ],
    ];

    return $build;
  }

}

<?php

namespace Drupal\reading_progress_bar;

/**
 * Various constants.
 */
class ReadingProgressBarConstants {
  const DEFAULT_HEIGHT = '5px';
  const DEFAULT_COLOR = '2579d9';
  const DEFAULT_BACKGROUND_COLOR = 'ba6b11';
  const DEFAULT_BORDER = 'none';
  const DEFAULT_MINIMUM_RATIO = 3;
  const DEFAULT_HIDE_DELAY = 3000;
}
